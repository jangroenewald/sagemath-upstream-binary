#!/bin/bash

#make changes to debian/* if needed
#For upgrade from 5.4.1 to 5.5, remove rm -rf /usr/lib/sage from postinst; leftover from 5.1
#dh_link in debian/rules for mathjax folder to symlink change in 6.6

export VERSION=`ls /home/jan/src/sagemath-upstream-binary/upstream|cut -d\- -f2,2|head -1`
echo "VERSION="${VERSION}
cd /home/jan/src/sagemath-upstream-binary/sagemath-upstream-binary
rm -rf i386 amd64
lrzuntar -O . ../upstream/sage-${VERSION}-i686-Linux-Ubuntu_14.04_32_bit.tar.lrz
mv sage-${VERSION}-i686-Linux i386
lrzuntar -O . ../upstream/sage-${VERSION}-x86_64-Linux-Ubuntu_14.04_64_bit.tar.lrz
mv sage-${VERSION}-x86_64-Linux amd64
tar -cJf ../sagemath-upstream-binary_${VERSION}.orig-i386.tar.xz i386
tar -cJf ../sagemath-upstream-binary_${VERSION}.orig-amd64.tar.xz amd64
tar -cvzf ../sagemath-upstream-binary_${VERSION}.orig.tar.gz --files-from /dev/null

# Test for dependencies # NEW UNTESTED 
cd amd64/local/bin/
E=`./sage -sh ../../../../get-deps.sh`
test "$E"x = "libc6, libexpat1, libfontconfig1, libgcc1, libgfortran3, libgomp1, libquadmath0, libssl1.0.0, libstdc++6"x || "echo 'Dependencies changed' && exit"
cd ../../..

# Fix icon path
cd debian
ICONTMP=`find ../amd64/local/lib/python/site-packages -name icon48x48.png`
ICON="/usr/lib/sagemath${ICONTMP#../amd64}"
head -n-1 sagemath.desktop > sagemath.desktop.new
echo -n "Icon=" >> sagemath.desktop.new
echo ${ICON} >> sagemath.desktop.new
mv sagemath.desktop.new sagemath.desktop

##debchange -i ## replaced by below non-interactive
cat > changelog.new << EOF
sagemath-upstream-binary (${VERSION}) trusty; urgency=low

  * New upstream release ${VERSION}

EOF
echo " -- Jan Groenewald <jan@aims.ac.za>  `date +\"%a, %d %b %Y %T %z\"`" >> changelog.new
echo >> changelog.new
cat changelog >> changelog.new
mv changelog.new changelog
cd ..

# Build the debs and source, unsigned; then sign them
debuild --no-lintian -b -us -uc
debuild --no-lintian -ai386 -b -us -uc
debuild --no-lintian -S -us -uc
## Build the source and force including orig files. For when a build failed and I want to rebuild same version number but debuild thinks that version is already built.
##debuild --no-lintian -S -us -uc -sa
echo "debsign -k'Jan Groenewald (www.aims.ac.za) <jan@aims.ac.za>' -S"
debsign -k'Jan Groenewald (www.aims.ac.za) <jan@aims.ac.za>' -S
# Upload the source package
echo "Test the package!"
echo "unset ftp_proxy; dput ppa:aims/sagemath sagemath-upstream-binary_${VERSION}_source.changes"

# Just notes
#debsign -kD6B0C242 sagemath-upstream-binary_4.8-10.04.3-0ubuntu2_source.changes
#debuild --no-lintian -us -uc | tee ../debuild.log
#debuild --no-lintian -ai386 -us -uc | tee ../debuild-i386.log
#dput ppa:aims/sagemath sagemath-upstream-binary_4.8-10.04.3-0ubuntu2_source.changes
